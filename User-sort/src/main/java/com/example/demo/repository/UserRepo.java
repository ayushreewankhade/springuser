package com.example.demo.repository;

import java.util.List;

//import javax.transaction.Transactional;


import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

//import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>  {

	//@Modifying
	//@Transactional
	//@Query(value= "SEECT * FROM Users ORDER BY name ASC", nativeQuery=true)
	//List<UserDto> findAllByOrderByNameAsc();
	List<User> findAllByOrderByNameAsc();
	
}
