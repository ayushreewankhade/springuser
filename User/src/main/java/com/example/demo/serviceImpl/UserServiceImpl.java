package com.example.demo.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;

import com.example.demo.repository.UserRepo;
import com.example.demo.service.UserService;
//import com.springbootproject.exceptions.ResourceNotFoundException;

@Component //@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepo userRepo;
	
	
	
	@Autowired
	private ModelMapper modelMapper;
	
	//private User dtoToUser(UserDto userDto)
	//{
	//	//convert dto to user --1 add source class?,class object add 
	//	User user= this.modelMapper.map(userDto,User.class );
	//	return user;
	//}

	private UserDto userToDto(User user)
	{
		UserDto userDto=this.modelMapper.map(user,UserDto.class);
		return userDto;
	}
	
	@Override
	public User creatUser(UserDto userDto) {
		User user= new User();
		user.setId(userDto.getId());
		user.setName(userDto.getName());
		user.setAddress(userDto.getAddress());
		return this.userRepo.save(user);
	}

	@Override
	public void updateUser(UserDto userDto, Integer id) {
		User user= this.userRepo.findById(id).orElseThrow();
		user.setName(userDto.getName());
		userRepo.save(user);
		
	}

	@Override
	public UserDto getUserById(Integer userId) {
		User user= this.userRepo.findById(userId).orElseThrow();
		return this.userToDto(user);
	}

	@Override
	public List<UserDto> getAllUser() {
		List<User> user= this.userRepo.findAll();
		List<UserDto>save=user.stream().map(e  -> this.userToDto(e)).collect(Collectors.toList());
		return save;
	}

	@Override
	public void deleteUser(Integer userId) {
		//this.userRepo.findById(userId).orElseThrow();
		this.userRepo.deleteById(userId);
		
	}

	//@Override
	//public List<User> findAllOrderByNameAsc() {  //
		
	//	return this.usernameRepo.findAllOrderByNameAsc();  //
	//}

	
	
}
