package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.example.demo.interceptor.TestInterceptor;

@Configuration
public class TestConfig implements WebMvcConfigurer {
	
 @Autowired
 TestInterceptor interceptor;
 
 @Override
 public void addInterceptors(InterceptorRegistry registry) {
  // this interceptor will be applied to all URLs
  registry.addInterceptor(interceptor);
 } 
}
