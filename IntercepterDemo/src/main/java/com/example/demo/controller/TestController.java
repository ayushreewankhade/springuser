package com.example.demo.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@PostMapping(value = "/testApiOne")
	public Map<String, Object> numberOneAPI(@RequestBody Map<String, Object> request) throws Exception {

		return request;
	}

	@PostMapping(value = "/testApiTwo")
	public Map<String, Object> numberTwoAPI(@RequestBody Map<String, Object> request) throws Exception {

		return request;
	}
	
	@RequestMapping("/test")
	 public String home() {
	  System.out.println("Inside handler Method");
	  return "Hello World !! Start using Interceptors";
	 }
}
	

